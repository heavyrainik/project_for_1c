Алексей Соболев Б05-823
sobolev.as@phystech.edu

Как запустить:

virtualenv --python=python3 venv
source venv/bin/activate

*установить необходимые библиотеки*

export FLASK_APP=main.py
flask run

Идея решения: 
Делаем базу данных и интерефейс, чтобы ей пользоваться!

Концепт:
1. Создаем базу данных "Orders"
2. Взаимодействовать с ней можно двумя способами:
    1) создание новой записи
    2) изменение уже текущей по номеру заказа

Другие полезные функции:
1) личный вход для каждого сотрудника
2) возможность искать заказы по ключевым словам
3) возможнсть удалять заказы
4) сортировки заказов по столбцам базы
5) группировка заказов по особому признаку

Это все я не успел, потому что увлекся джинжой, чтобы красиво отображать изменения