from app import db


class Order(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    client = db.Column(db.String)
    address = db.Column(db.String)
    contacts = db.Column(db.String)
    contact_number = db.Column(db.String)
    price = db.Column(db.Integer)
    status = db.Column(db.Integer)

    def save(self):
        db.session.add(self)
        db.session.commit()
        self.chat.process_id = self.id
        self.chat.save()
        return self.id
