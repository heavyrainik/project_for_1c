from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

from config import Config
template_dir = os.path.abspath('/templates')

app = Flask(__name__, template_folder="../app/templates")
app.config.from_object(Config)

db = SQLAlchemy(app)
db.create_all()

from app import business_logic