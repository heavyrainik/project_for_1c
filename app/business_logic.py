from app import app
from app.forms import OrderForm, OrderStatusChangeForm
from app.db_models import Order

from flask import render_template


@app.route('/', methods=['GET', 'POST'])
def index():
    form = OrderForm()

    if form.validate_on_submit():
        order = OrderForm(client=form.client.data, address=form.address.data,
                          contacts=form.contacts.data, contact_number=form.contact_number.data,
                          price=form.price.data, status=form.status.data)

        Order.save(order)
        return render_template('create_order.html', title='Создать заказ', form=form, res='success')

    return render_template('create_order.html', title='Создать заказ', form=form, res='failure')


@app.route('/change_status', methods=['GET', 'POST'])
def change_order_status():
    form = OrderStatusChangeForm()

    if form.validate_on_submit():
        Order.query.filter_by(id=form.order_id.data).update(
            {'status': form.status.data})
        return render_template('change_status.html', title='Создать заказ', form=form, res='success')

    return render_template('change_status.html', title='Создать заказ', form=form, res='failure')


@app.route('/all', methods=['GET', 'POST'])
def display_all_orders():
    return
