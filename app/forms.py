from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, SubmitField


class OrderForm(FlaskForm):
    client = StringField('client')
    address = StringField('address')
    contacts = StringField('contacts')
    contact_number = StringField('contact_number')
    price = StringField('price')
    status = SelectField('status', choices=[(1, 'обработка'),
                                             (2, 'сборка'),
                                             (3, 'доставка'),
                                             (4, 'отменен'),
                                             (5, 'завершен')])

    submit = SubmitField('Create')


class OrderStatusChangeForm(FlaskForm):
    order_id = StringField('order_id')
    status = SelectField('status', choices=[(1, 'обработка'),
                                            (2, 'сборка'),
                                            (3, 'доставка'),
                                            (4, 'отменен'),
                                            (5, 'завершен')])

    submit = SubmitField('Create')
